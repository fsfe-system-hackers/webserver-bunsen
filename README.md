# FSFE Webserver

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/webserver/00_README)

## Install

Clone this repo:

```
git clone --recurse-submodules git@git.fsfe.org:fsfe-system-hackers/webserver.git
```

## Usage

Update the inventory submodule to reflect the newest changes to the list of our
/generate_inventory and the groups that they are in:

```
git submodule update --remote inventory
```

Deploy/update all webservers:

```
ansible-playbook playbook.yaml
```

Deploy/update only one webserver:

```
ansible-playbook -l bunsen.fsfeurope.org playbook.yaml
```

### Tags

There are some ansible tags you can use to only run specific tasks:

* `apache` (all typical changes in apache, but no directory creation or so)
* `certs` (all things regarding TLS certificates, certbot etc)
* `php` (PHP configuration)


### Host Vars

The hosts' variables play an essential role. Some are self-explanatory, some are
more complex

Host-wide config:
* `onion_address` defines the Onion/Tor address for that host. Currently only
  applies to fsfe.org.
* `ip_proxy` defines whether the whole server – so all primary and alias domains
  – is behind our ip-proxy. In this case, some extra settings have to be made,
  and the Apache LogFormat is different. Unfortunately, this cannot be set on a
  vhost basis.

Each entry under `domains` is an own vhost and certificate. The parent is the
primary domain, e.g. `fsfe.org` or `freeyourandroid.org`.
* `template` defines the type of the apache2 vhost config to be used (see under
  `/roles/apache2-site/templates`).
* `aliases` are domains that are aliases to the primary domain (ServerAlias).
* `mode` is a special declaration to differ fsfe.org and test.fsfe.org, and not
  used for templates other than "fsfe"
* `target` applies to "redirection" vhosts, so the URL all requests are pointed
  to.
* `www_dir` is for vhosts that are served directly from a directory
  ("subdirectory" template).
* `acme_challenge` defines how TLS certs are gathered. Default is http-01, but
  `dns-01` triggers the use of the acne-dns-client role.
* `rewrites` can define paths for "subdirectory" templates which redirect to the
  primary domain.
* `includes` defines special Apache2 config files that should be included.

## Important Notes

* All domains/aliases that are served by more than one host/VM MUST have
  `acme_challenge: "dns-01"`. Otherwise, gathering a new or renewerd cert will
  fail sooner or later. This then applies all aliases of the primary domain.
